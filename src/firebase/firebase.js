import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyDih8deWScwwjkbsFEZNYrg4EpOVulCEgQ",
  authDomain: "test-app-f13ca.firebaseapp.com",
  databaseURL: "https://test-app-f13ca.firebaseio.com",
  projectId: "test-app-f13ca",
  storageBucket: "test-app-f13ca.appspot.com",
  messagingSenderId: "829749782697"
};

firebase.initializeApp(config);

const database = firebase.database();

export { firebase, database as default };
