import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import CircularProgress from '@material-ui/core/CircularProgress';
import configureStore from "./store/configureStore";
import AppRouter, { history } from "./router/appRouter";
import "./styles/main.scss";
import { firebase } from "./firebase/firebase";
import { startSetItems, login, logout, setOrderBy } from "./store/actions";

const store = configureStore();

const TestApp = () => (
  <Fragment>
    <CssBaseline />
    <Provider store={store}>
      <AppRouter />
    </Provider>
  </Fragment>
);

let rendering = false;
const renderTestApp = () => {
  if (!rendering) {
    ReactDOM.render(<TestApp />, document.getElementById("root"));
    rendering = true;
  }
};

ReactDOM.render(<CircularProgress className="login" color="primary"
          size={120}  />, document.getElementById("root"));

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    store.dispatch(login(user.uid));
    store.dispatch(setOrderBy());
    store.dispatch(startSetItems()).then(() => {
      renderTestApp();
    });
  } else {
    store.dispatch(logout());
    store.dispatch(startSetItems()).then(() => {
      renderTestApp();
    });
    history.push("/");
  }
});
