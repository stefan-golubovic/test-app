import React, { Fragment } from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Icon from "@material-ui/core/Icon";
import DeleteForeverIcon from "@material-ui/icons/Delete";
import { Link } from "react-router-dom";
import { history } from "../router/appRouter";
import { startRemoveItem } from "../store/actions";

const ItemDetails = props => {
  let btns = <Link to="/">Back</Link>;
  if (props.isAuth && props.isAuth === props.item.authorId) {
    btns = (
      <Fragment>
        <Button
          component={Link}
          to={`/edit/${props.item.id}`}
          color="primary"
          style={{ width: "20px" }}
        >
          <Icon>edit</Icon>
        </Button>

        <Button
          onClick={() => props.onRemoveItem(props.item.id)}
          color="secondary"
          style={{ width: "20px" }}
        >
          <DeleteForeverIcon />
        </Button>
      </Fragment>
    );
  }
  return (
    <Paper>
      <div className="itemDetails">
        <div className="itemDetails__header">
          <h2 style={{ flexGrow: 1 }}>{props.item.title}</h2>
          {btns}
        </div>
        <h3 style={{ fontWeight: "300" }}>Amount: {props.item.amount}</h3>
        <p>{props.item.description}</p>
      </div>
    </Paper>
  );
};

const mapStateToProps = (state, props) => {
  return {
    item: state.itemsReducer.items.find(
      item => item.id === props.match.params.id
    ),
    isAuth: state.authReducer.uid
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onRemoveItem: id => {
      dispatch(startRemoveItem(id));
      history.push("/");
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemDetails);
