import React from "react";
import { connect } from "react-redux";
import { startAddItem } from "../store/actions";
import Form from "../containers/Form";
import { history } from "../router/appRouter";

const AddItem = props => (
  <div>
    <Form onSubmit={props.onStartAddItem} title="Add Item" />
  </div>
);

const mapDispatchToProps = dispatch => ({
  onStartAddItem: item => {
    dispatch(startAddItem(item));
    history.push("/");
  }
});

export default connect(
  null,
  mapDispatchToProps
)(AddItem);
