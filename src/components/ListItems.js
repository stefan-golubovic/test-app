import React from "react";
import { Link } from "react-router-dom";
import { Droppable, Draggable } from "react-beautiful-dnd";
import Paper from "@material-ui/core/Paper";
import moment from "moment";

const ListItems = props => {
  let listItems = <p>There are no items</p>;
  if (props.items.length) {
    listItems = props.items.map((item, i) => {
      return (
        <Draggable draggableId={item.id} key={item.id} index={i}>
          {provided => (
            <div
              className="list-items__item"
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <Paper>
                <Link
                  to={`/itemDetails/${item.id}`}
                  style={{ textDecoration: "none", marginLeft: "30px" }}
                >
                  <h3>
                    {item.title}
                    <span style={{ fontWeight: "200" }}>
                      {" "}
                      | Amount:
                      <strong> {item.amount}</strong>
                    </span>
                  </h3>
                  <p>
                    Created at{" "}
                    {moment(item.createdAt).format("HH:mm:ss DD-MM-YYYY ")}
                    <br />
                    Updated at{" "}
                    {moment(item.updatedAt).format("HH:mm:ss DD-MM-YYYY ")}
                  </p>
                </Link>
              </Paper>
            </div>
          )}
        </Draggable>
      );
    });
  }
  return (
    <Droppable droppableId="c1">
      {provided => (
        <div
          className="list-items"
          ref={provided.innerRef}
          {...provided.droppableProps}
        >
          {listItems}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
};

export default ListItems;
