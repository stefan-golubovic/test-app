export const ADD_ITEM = "ADD_ITEM";
export const START_REMOVE_ITEM = "START_REMOVE_ITEM";
export const REMOVE_ITEM = "REMOVE_ITEM";
export const START_UPDATE_ITEM = "START_UPDATE_ITEM";
export const UPDATE_ITEM = "UPDATE_ITEM";
export const START_SET_ITEMS = "START_SET_ITEMS";
export const SET_ITEMS = "SET_ITEMS";

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

export const AMOUNT_ASC = "AMOUNT_ASC";
export const AMOUNT_DESC = "AMOUNT_DESC";
export const CREATED_AT_ASC = "CREATED_AT_ASC";
export const CREATED_AT_DESC = "CREATED_AT_DESC";
export const UPDATED_AT_ASC = "UPDATED_AT_ASC";
export const UPDATED_AT_DESC = "UPDATED_AT_DESC";
export const SELF_DEFINED_POSITIONS = "SELF_DEFINED_POSITIONS";
