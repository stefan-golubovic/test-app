import * as actionTypes from "./actionTypes";
import database from "../../firebase/firebase";

export const addItem = (item, itemPositions) => {
  return { type: actionTypes.ADD_ITEM, item, itemPositions };
};

export const startAddItem = item => {
  return (dispatch, getState) => {
    const uid = getState().authReducer.uid;
    let itemId = "";
    // add Item to spesific User
    database
      .ref(`users/${uid}/items`)
      .push(item)
      .then(snapshot => {
        itemId = snapshot.key;
        const items = getState();
        const itemPositions = items.itemsReducer.itemPositions || [];

        dispatch(
          addItem({ id: itemId, authorId: uid, ...item }, [
            itemId,
            ...itemPositions
          ])
        );

        // add new itemPositions to all users
        database
          .ref("users")
          .once("value")
          .then(users => {
            users.forEach(user => {
              const itemsOrder = user.child("itemsOrder");
              if (!itemsOrder.val()) {
                console.log(itemPositions);
                database
                  .ref(`users/${user.key}/itemsOrder`)
                  .set([itemId].concat(itemPositions));
              } else {
                console.log(itemsOrder.val());
                database
                  .ref(`users/${user.key}/itemsOrder`)
                  .set([itemId].concat(itemsOrder.val()));
              }
            });
          });
      });
  };
};

export const removeItem = id => {
  return {
    type: actionTypes.REMOVE_ITEM,
    id
  };
};

export const startRemoveItem = id => {
  return (dispatch, getState) => {
    const uid = getState().authReducer.uid;
    database
      .ref(`users/${uid}/items/${id}`)
      .remove()
      .then(() => {
        database
          .ref("users")
          .once("value")
          .then(users => {
            users.forEach(user => {
              const itemsOrder = user.child("itemsOrder");
              const index = itemsOrder.val().findIndex(itemId => itemId === id);
              database.ref(`users/${user.key}/itemsOrder/${index}`).remove();
            });
          });
        dispatch(removeItem(id));
      });
  };
};

export const updateItem = (id, updates) => {
  return { type: actionTypes.UPDATE_ITEM, id, updates };
};

export const startUpdateItem = (id, updates) => {
  return (dispatch, getState) => {
    const uid = getState().authReducer.uid;
    database
      .ref(`users/${uid}/items/${id}`)
      .update(updates)
      .then(() => dispatch(updateItem(id, updates)));
  };
};

export const setItems = (items, itemPositions) => {
  return { type: actionTypes.SET_ITEMS, items, itemPositions };
};

export const startSetItems = () => {
  return (dispatch, getState) => {
    return database
      .ref("users")
      .once("value")
      .then(users => {
        const allItems = [];
        const itemPositions = [];
        users.forEach(user => {
          const items = user.child("items");
          const userId = user.key;

          items.forEach(item => {
            allItems.push({
              id: item.key,
              authorId: userId,
              ...item.val()
            });
          });
        });

        const uid = getState().authReducer.uid;

        database
          .ref(`users/${uid}/itemsOrder`)
          .once("value")
          .then(itemsOrder => {
            itemsOrder.forEach(itemId => {
              itemPositions.push(itemId.val());
            });
            if (itemPositions.length) {
              dispatch(setItems(allItems, itemPositions));
            } else {
              allItems.forEach(item => {
                itemPositions.push(item.id);
              });
              dispatch(setItems(allItems, itemPositions));
            }
          });
      });
  };
};

export const updatePositions = newPositions => {
  return {
    type: actionTypes.SELF_DEFINED_POSITIONS,
    newPositions
  };
};

export const startUpdatePositions = newPositions => {
  return (dispatch, getState) => {
    const uid = getState().authReducer.uid;
    database
      .ref(`users/${uid}/itemsOrder`)
      .set([...newPositions])
      .then(dispatch(updatePositions(newPositions)));
  };
};
