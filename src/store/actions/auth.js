import * as actionTypes from "./actionTypes";
import { firebase } from "../../firebase/firebase";

export const login = uid => ({
  type: actionTypes.LOGIN,
  uid
});

export const logout = () => ({
  type: actionTypes.LOGOUT
});

export const startLogout = () => {
  return () => {
    return firebase.auth().signOut();
  };
};
