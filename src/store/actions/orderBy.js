import * as actionTypes from "./actionTypes";
import database from "../../firebase/firebase";

export const orderBy = order => {
  switch (order) {
    case "amountASC":
      return { type: actionTypes.AMOUNT_ASC };
    case "amountDESC":
      return { type: actionTypes.AMOUNT_DESC };
    case "createdAtASC":
      return { type: actionTypes.CREATED_AT_ASC };
    case "createdAtDESC":
      return { type: actionTypes.CREATED_AT_DESC };
    case "updatedAtASC":
      return { type: actionTypes.UPDATED_AT_ASC };
    case "updatedAtDESC":
      return { type: actionTypes.UPDATED_AT_DESC };

    default:
      return { type: actionTypes.AMOUNT_ASC };
  }
};

export const startOrderBy = orderType => {
  return (dispatch, getState) => {
    const uid = getState().authReducer.uid;
    database
      .ref(`users/${uid}`)
      .update({ orderBy: orderType })
      .then(() => {
        dispatch(orderBy(orderType));
      });
  };
};

export const setOrderBy = () => {
  return (dispatch, getState) => {
    const uid = getState().authReducer.uid;
    return database
      .ref(`users/${uid}/orderBy`)
      .once("value")
      .then(snapshot => {
        dispatch(orderBy(snapshot.val()));
      });
  };
};
