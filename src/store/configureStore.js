import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import itemsReducer from "./reducers/items";
import authReducer from "./reducers/auth";
import orderByReducer from "./reducers/orderBy";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(
    combineReducers({
      itemsReducer,
      authReducer,
      orderByReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
  );

  return store;
};
