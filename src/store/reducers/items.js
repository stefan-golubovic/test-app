import * as actionType from "../actions/actionTypes";

const initState = {
  items: [],
  itemPositions: []
};
const items = (state = initState, action) => {
  switch (action.type) {
    case actionType.ADD_ITEM:
      return {
        ...state,
        items: [...state.items, action.item],
        itemPositions: [...action.itemPositions]
      };

    case actionType.REMOVE_ITEM:
      const items = state.items.filter(item => item.id !== action.id);
      return {
        ...state,
        items
      };

    case actionType.UPDATE_ITEM:
      const updatedItems = state.items.map(item => {
        if (item.id === action.id) {
          return Object.assign({}, item, action.updates);
        } else {
          return item;
        }
      });
      return {
        ...state,
        items: updatedItems
      };

    case actionType.SET_ITEMS:
      return {
        ...state,
        items: [...action.items],
        itemPositions: [...action.itemPositions]
      };

    case actionType.SELF_DEFINED_POSITIONS:
      return {
        ...state,
        itemPositions: action.newPositions
      };

    default:
      return state;
  }
};

export default items;
