import * as actionType from "../actions/actionTypes";

const auth = (state = { uid: false }, action) => {
  switch (action.type) {
    case actionType.LOGIN:
      return {
        uid: action.uid
      };

    case actionType.LOGOUT:
      return { uid: false };

    default:
      return state;
  }
};

export default auth;
