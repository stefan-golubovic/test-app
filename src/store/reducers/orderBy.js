import * as actionType from "../actions/actionTypes";

const orderBy = (state = { orderType: "amountASC" }, action) => {
  switch (action.type) {
    case actionType.AMOUNT_ASC:
      return {
        orderType: "amountASC"
      };
    case actionType.AMOUNT_DESC:
      return {
        orderType: "amountDESC"
      };
    case actionType.CREATED_AT_ASC:
      return {
        orderType: "createdAtASC"
      };
    case actionType.CREATED_AT_DESC:
      return {
        orderType: "createdAtDESC"
      };
    case actionType.UPDATED_AT_ASC:
      return {
        orderType: "updatedAtASC"
      };
    case actionType.UPDATED_AT_DESC:
      return {
        orderType: "updatedAtDESC"
      };

    default:
      return state;
  }
};

export default orderBy;
