import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import { startLogout } from "../store/actions";

const Navbar = props => {
  let logoutBtn = null;
  if (props.isAuth) {
    logoutBtn = (
      <Button onClick={props.onStartLogout} color="inherit">
        Log out
      </Button>
    );
  }
  return (
    <AppBar>
      <div className="navbar__container">
        <Link to="/" style={{ textDecoration: "none" }}>
          <h1>Test App</h1>
        </Link>
        <div>{logoutBtn}</div>
      </div>
    </AppBar>
  );
};

const mapDispatchToProps = dispatch => {
  return { onStartLogout: () => dispatch(startLogout()) };
};

export default connect(
  null,
  mapDispatchToProps
)(Navbar);
