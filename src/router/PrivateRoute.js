import React, { Fragment } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Navbar from "../layouts/Navbar";

const PrivateRoute = ({ isAuth, component: Component, ...rest }) => (
  <Fragment>
    <Navbar isAuth={isAuth} />
    <div className="wrapper__container">
      <Route
        {...rest}
        render={props => {
          return isAuth ? <Component {...props} /> : <Redirect to="/" />;
        }}
      />
    </div>
  </Fragment>
);

const mapStateToProps = state => {
  return {
    isAuth: state.authReducer.uid
  };
};

export default connect(mapStateToProps)(PrivateRoute);
