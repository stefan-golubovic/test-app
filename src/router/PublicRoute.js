import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";
import Navbar from "../layouts/Navbar";

const PublicRoute = ({ isAuth, component: Component, ...rest }) => (
  <Fragment>
    <Navbar isAuth={isAuth} />
    <div className="wrapper__container">
      <Route {...rest} render={props => <Component {...props} />} />
    </div>
  </Fragment>
);

const mapStateToProps = state => {
  return {
    isAuth: state.authReducer.uid
  };
};

export default connect(mapStateToProps)(PublicRoute);
