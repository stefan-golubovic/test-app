import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import AddItem from "../components/AddItem";
import EditItem from "../containers/EditItem";
import Dashboard from "../containers/Dashboard";
import ItemDetails from "../components/ItemDetails";
import NotFoundPage from "../components/NotFoundPage";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

export const history = createHistory();

const appRouter = () => {
  return (
    <Router history={history}>
      <div className="wrapper">
        <Switch>
          <PublicRoute path="/" component={Dashboard} exact />
          <PublicRoute path="/itemDetails/:id" component={ItemDetails} exact />
          <PrivateRoute path="/edit/:id" component={EditItem} exact />
          <PrivateRoute path="/add" component={AddItem} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    </Router>
  );
};

export default appRouter;
