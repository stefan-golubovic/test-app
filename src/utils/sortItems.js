const sortItems = (items, sortBy) => {
  return items.sort((a, b) => {
    switch (sortBy) {
      case "amountASC":
        return a.amount < b.amount ? -1 : 1;
      case "amountDESC":
        return a.amount < b.amount ? 1 : -1;

      case "createdAtASC":
        return a.createdAt < b.createdAt ? 1 : -1;
      case "createdAtDESC":
        return a.createdAt < b.createdAt ? -1 : 1;

      case "updatedAtASC":
        return a.updatedAt < b.updatedAt ? 1 : -1;
      case "updatedAtDESC":
        return a.updatedAt < b.updatedAt ? -1 : 1;

      default:
        return a.createdAt < b.createdAt ? -1 : 1;
    }
  });
};

export default sortItems;
