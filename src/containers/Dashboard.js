import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
// import Select from "@material-ui/core/Select";
// import MenuItem from "@material-ui/core/MenuItem";
import { DragDropContext } from "react-beautiful-dnd";
import { firebase } from "../firebase/firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import { startOrderBy, startUpdatePositions } from "../store/actions";
import ListItems from "../components/ListItems";
// import sortItems from "../utils/sortItems";
// import orderItems from "../utils/orderItems";

class Dashboard extends PureComponent {
  state = {
    column: {
      itemIds: []
    }
  };
  onSortChange = e => {
    this.props.onOrderBy(e.target.value);
  };

  onDragEnd = result => {
    const { destination, source, draggableId } = result;
    if (!destination) {
      return;
    }

    if (destination.index === source.index) {
      return;
    }

    const newItemPositions = Array.from(this.props.itemPositions);
    newItemPositions.splice(source.index, 1);
    newItemPositions.splice(destination.index, 0, draggableId);

    if (this.props.isAuth) {
      this.props.onUpdatePositions(newItemPositions);
    }
  };

  render() {
    let btn = null;
    // let selectInput = null;

    if (this.props.isAuth) {
      btn = (
        <Button component={Link} to="/add" color="primary">
          {" "}
          <Icon style={{ fontSize: 36 }} color="inherit">
            add_circle
          </Icon>
        </Button>
      );

      // selectInput = (
      //   <Select value={this.props.orderBy} onChange={this.onSortChange}>
      //     <MenuItem value="selfDefined">Self-defined D&D</MenuItem>
      //     <MenuItem value="amountASC">Amount ASC</MenuItem>
      //     <MenuItem value="amountDESC">Amount DESC</MenuItem>
      //     <MenuItem value="createdAtASC">Created At ASC</MenuItem>
      //     <MenuItem value="createdAtDESC">Created At DESC</MenuItem>
      //     <MenuItem value="updatedAtASC">Updated At ASC</MenuItem>
      //     <MenuItem value="updatedAtDESC">Updated At DESC</MenuItem>
      //   </Select>
      // );
    } else {
      btn = (
        <StyledFirebaseAuth
          uiConfig={{
            signInFlow: "popup",
            signInOptions: [
              firebase.auth.GoogleAuthProvider.PROVIDER_ID,
              firebase.auth.GithubAuthProvider.PROVIDER_ID
            ]
          }}
          firebaseAuth={firebase.auth()}
        />
      );
    }
    // const sortedItems = sortItems(this.props.items, this.props.orderBy);
    // console.log(this.props.items);
    const orderedItems = this.props.itemPositions.map(itemId => {
      return this.props.items.find(item => item.id === itemId);
    });

    return (
      <Fragment>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            paddingTop: "20px"
          }}
        >
          {this.props.isAuth && <p>D&D has enabled</p>}
          {/* {selectInput} */}
          {btn}
        </div>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <ListItems
            items={orderedItems}
            itemPositions={this.props.itemPositions}
          />
        </DragDropContext>

        {/* <ListItems items={sortedItems} /> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.itemsReducer.items,
    itemPositions: state.itemsReducer.itemPositions,
    isAuth: state.authReducer.uid,
    orderBy: state.orderByReducer.orderType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onOrderBy: type => dispatch(startOrderBy(type)),
    onUpdatePositions: newPositions =>
      dispatch(startUpdatePositions(newPositions))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
