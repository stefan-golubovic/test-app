import React, { Component } from "react";
import { connect } from "react-redux";
import { startUpdateItem } from "../store/actions";
import Form from "./Form";

class EditItem extends Component {
  onSubmit = updates => {
    this.props.onUpdateItem(this.props.item.id, updates);
    this.props.history.push("/");
  };
  componentWillMount() {
    if (this.props.item.authorId !== this.props.isAuth) {
      this.props.history.push("/");
    }
  }
  render() {
    return (
      <div>
        <Form
          onSubmit={this.onSubmit}
          item={this.props.item}
          title="Update Item"
        />
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    item: state.itemsReducer.items.find(
      item => item.id === props.match.params.id
    ),
    isAuth: state.authReducer.uid
  };
};

const mapDispatchToProps = dispatch => ({
  onUpdateItem: (id, updates) => dispatch(startUpdateItem(id, updates))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditItem);
