import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import { history } from "../router/appRouter";

class Form extends Component {
  state = {
    title: this.props.item ? this.props.item.title : "",
    description: this.props.item ? this.props.item.description : "",
    amount: this.props.item ? this.props.item.amount : null,
    createdAt: this.props.item ? this.props.item.createdAt : null,
    updatedAt: this.props.item ? this.props.item.updatedAt : null,
    error: ""
  };

  onSubmit = e => {
    e.preventDefault();
    const titleVal = e.target.title.value;
    const descVal = e.target.description.value;
    const amountVal = parseInt(e.target.amount.value, 10);

    if (this.props.title === "Update Item") {
      const { title, description, amount } = this.state;
      if (
        title === titleVal &&
        description === descVal &&
        amount === amountVal
      ) {
        return history.push("/dashboard");
      }
    }

    if (!titleVal || !descVal || !amountVal) {
      this.setState(() => ({
        ...this.state,
        error: "Please fill in all fields."
      }));
    } else {
      this.setState({ error: "" });
      this.props.onSubmit({
        amount: amountVal,
        description: descVal,
        title: titleVal,
        createdAt: this.state.createdAt
          ? this.state.createdAt
          : moment().valueOf(),
        updatedAt: moment().valueOf()
      });
    }
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <TextField
          autoFocus
          id="Title"
          label="Title"
          name="title"
          margin="normal"
          placeholder="Title"
          defaultValue={this.state.title}
        />
        <br />
        <TextField
          id="number"
          label="Number"
          name="amount"
          defaultValue={this.state.amount}
          type="number"
          margin="normal"
        />
        <br />
        <br />
        <TextField
          id="textarea"
          label="With placeholder multiline"
          name="description"
          defaultValue={this.state.description}
          placeholder="Add description"
          multiline
          margin="normal"
        />
        <br />
        <Button type="Submit" variant="contained" color="primary">
          {this.props.title}
        </Button>
      </form>
    );
  }
}

export default Form;
